from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
import psycopg2
import psycopg2.extras

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:postgres@localhost/user'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

"""DB_HOST = "localhost"
DB_NAME = "user"
DB_USER = "postgres"
DB_PASS = "postgres"
conn = psycopg2.connect(dbname = DB_NAME, user = DB_USER, password = DB_PASS, host = DB_HOST,)"""

CORS(app)

db = SQLAlchemy(app)

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True)
    user_name = db. Column(db.String(100), nullable = False)
    user_phone = db.Column(db.Integer, nullable = False)
    user_email = db.Column(db.String(100), nullable = False)
    user_address = db.Column(db.String(100), nullable = False)


    def __repr__(self):
        return "<user %r>" % self.user_name

@app.route('/')
def index():
        return jsonify({"message":"Welcome to my site"})
        
        
@cross_origin()
@app.route('/user', methods = ['POST'])
def create_user():
    user_data = request.json

    user_name = user_data['user_name']
    user_phone = user_data['user_phone']
    user_email = user_data['user_email']
    user_address = user_data['user_address']

    """{
    "id": 1,
    "user_name": "Shweta",
    "user_phone": "1234",
    "user_email": "soni",
    "user_address": "gonda up"}"""

    user = User(user_name=user_name, user_phone = user_phone,user_email = user_email,user_address = user_address)
    
    db.session.add(user)
    db.session.commit()
    

    return jsonify({"success": True,"response":"user added"})



@cross_origin()    
@app.route('/getuser', methods = ['GET'])
def getuser():
     all_user = []
     user = User.query.all()
     for user in user:
          results = {
                    "user_id":user.id,
                    "user_name":user.user_name,
                    "user_phone":user.user_phone,
                    "user_email":user.user_email,
                    "user_address":user.user_address, }
          all_user.append(results)

     return jsonify(
            {
                "success": True,
                "user": all_user,
                #"total_user":len(user),

            }
        )

if __name__ == '__main__':
  app.run(debug=True)